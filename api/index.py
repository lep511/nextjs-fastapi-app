from typing import Union
from fastapi import FastAPI
import google.generativeai as genai
import os

api_key = os.environ.get("GOOGLE_API_KEY")
genai.configure(api_key=api_key)
app = FastAPI()

# Set up the model
generation_config = {
  "temperature": 1,
  "top_p": 0.95,
  "top_k": 0,
  "max_output_tokens": 8192,
}

model = genai.GenerativeModel(model_name="gemini-1.5-pro-latest",
                              generation_config=generation_config
)

@app.get("/api/python")
def hello_world():
    prompt_parts = [
        "Which athletes won the gold medal in curling at the 2022 Winter Olympics?\n",
    ]
    response = model.generate_content(prompt_parts)
    return {"message": response.text}

@app.get("/api/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}